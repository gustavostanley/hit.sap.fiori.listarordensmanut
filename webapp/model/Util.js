sap.ui.define([], function () {
	"use strict";

	return {
		formatStatus: function (sStatus) {
			let sStat;

			if (sStatus !== null) {
				switch (true) {
				case sStatus.includes("ENCR"):
					sStat = "ENCR";
					break;
				case sStatus.includes("SUFM") || sStatus.includes("SUMO") || sStatus.includes("SUMT") || sStatus.includes("SUUM"):
					sStat = "SUSP";
					break;
				case sStatus.includes("INIC"):
					sStat = "INIC";
					break;
				default:
					sStat = "";
				}
			}
			return sStat;
		}
	};
});