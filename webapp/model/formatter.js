sap.ui.define([
	"hit/sap/fiori/ListarOrdensManut/model/Util"
], function (Util) {
	"use strict";

	return {
		criticidadeText: function (sCrit) {
			var resourceBundle = this.getView().getModel("i18n").getResourceBundle();
			switch (sCrit) {
			case "1":
				return resourceBundle.getText("lblCrit1");
			case "2":
				return resourceBundle.getText("lblCrit2");
			case "3":
				return resourceBundle.getText("lblCrit3");
			case "4":
				return resourceBundle.getText("lblCrit4");
			default:
				return sCrit;
			}
		},

		criticidadeState: function (sCrit) {
			switch (sCrit) {
			case "1":
				return "Error";
			case "2":
				return "Warning";
			case "3":
				return "Success";
			case "4":
				return "None";
			default:
				return "None";
			}
		},

		tipoText: function (sTipo) {
			var resourceBundle = this.getView().getModel("i18n").getResourceBundle();
			switch (sTipo) {
			case "C":
				return resourceBundle.getText("txtCorretiva");
			case "P":
				return resourceBundle.getText("txtPreventiva");
			default:
				return sTipo;
			}
		},

		formatDate: function (oDate) {
			jQuery.sap.require("sap.ui.core.format.DateFormat");
			if (oDate !== null && oDate !== "" && oDate !== undefined) {
				var oDateFormat = sap.ui.core.format.DateFormat.getDateTimeInstance({
					pattern: "dd/MM/yyyy"
				});
				if (typeof oDate !== "object") {
					oDate = new Date(parseInt(oDate.replace("/Date(", "")));
				}
				return oDateFormat.format(new Date(oDate));
			} else {
				return "";
			}
		},

		formatContactoProduto: function (sContacto) {
			var resourceBundle = this.getView().getModel("i18n").getResourceBundle();
			if (sContacto) {
				return resourceBundle.getText("lblPositivo");
			} else {
				return resourceBundle.getText("lblNegativo");
			}
		},

		formatNumOM: function (sNumOM, sStatus) {
			let oData = this.getView().getModel("device").getData();
			let sStat = Util.formatStatus(sStatus);
			if (sStat !== "" && oData.isPhone) {
				return `${sNumOM} - ${sStat}`;
			} else {
				return sNumOM;
			}
		},

		formatStatusText: function (sStatus) {
			let sStat = Util.formatStatus(sStatus);
			let sStatText;
			switch (sStat) {
			case "ENCR":
				sStatText = "Encerrada";
				break;
			case "SUSP":
				sStatText = "Suspensa";
				break;
			case "INIC":
				sStatText = "Iniciada";
				break;
			default:
				sStatText = "Não iniciada";
			}
			return sStatText;
		}
	};
});