/* global QUnit*/

sap.ui.define([
	"sap/ui/test/Opa5",
	"hit/sap/fiori/ListarOrdensManut/test/integration/pages/Common",
	"sap/ui/test/opaQunit",
	"hit/sap/fiori/ListarOrdensManut/test/integration/pages/App",
	"hit/sap/fiori/ListarOrdensManut/test/integration/navigationJourney"
], function (Opa5, Common) {
	"use strict";
	Opa5.extendConfig({
		arrangements: new Common(),
		viewNamespace: "hit.sap.fiori.ListarOrdensManut.view.",
		autoWait: true
	});
});