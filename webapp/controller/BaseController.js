sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History",
	"hit/sap/fiori/ListarOrdensManut/model/BarcodeScanner",
	"sap/m/MessageBox"
], function (Controller, History, BarcodeScanner, MessageBox) {
	"use strict";

	return Controller.extend("hit.sap.fiori.ListarOrdensManut.controller.BaseController", {

		oGlobalBusyDialog: null,
		stackLoading: [],

		getRouter: function () {
			return sap.ui.core.UIComponent.getRouterFor(this);
		},

		getModel: function (sName) {
			return this.getView().getModel(sName);
		},

		setModel: function (oModel, sName) {
			return this.getView().setModel(oModel, sName);
		},

		getResourceBundle: function () {
			return this.getOwnerComponent().getModel("i18n").getResourceBundle();
		},

		showLoading: function () {
			if (this.stackLoading === undefined || this.stackLoading.length === 0) {
				this.oGlobalBusyDialog = new sap.m.BusyDialog();
				this.oGlobalBusyDialog.open();
			}
			this.stackLoading.push(1);
		},

		closeLoading: function () {
			this.stackLoading.pop(1);
			if (this.stackLoading === undefined || this.stackLoading.length === 0) {
				this.oGlobalBusyDialog.close();
			}
		},

		navBack: function (sRoute) {
			var oHistory = History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();

			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				this.getRouter().navTo(sRoute, true);
			}
		},

		clearDataModel: function (sModelName) {
			let oModel = this.getModel(sModelName);
			let oData = oModel.getData();

			Object.keys(oData).forEach(key => {
				oData[key] = typeof oData[key] === "number" ? 0 : "";
				if (key === "showCameraIcon") {
					oData[key] = BarcodeScanner.isAvailable();
				}
			});
			oModel.setData(oData);
		},

		setFieldError: function (oField, type, text) {
			if (type === this.constants.error) {
				oField.setValueState(sap.ui.core.ValueState.Error);
				oField.setValueStateText(text);
				return true;
			} else {
				oField.setValueState(sap.ui.core.ValueState.None);
				return false;
			}
		},

		trataError: function (oError) {
			let sError = this.getResourceBundle().getText("msgError");
			let sFalhaRede = this.getResourceBundle().getText("msgFalhaRede");
			let sNoInternet = this.getResourceBundle().getText("msgNoInternet");
			let errorObj;
			try {
				errorObj = JSON.parse(oError.responseText);
			} catch (err) {

				errorObj = {
					error: {
						message: {
							value: sFalhaRede
						}
					}
				};

				if (oError.responseText === "") {
					errorObj.error.message.value += sNoInternet;
				} else {
					errorObj.error.message.value += oError.responseText.replace(/[\n\r]+/g, " ");
				}
			}
			MessageBox.error(sError + errorObj.error.message.value);
		},

		msgSucessoOuWarning: function (SucessoMsgCode, WarningMessage) {
			if (WarningMessage === null || WarningMessage === "") {
				let sText = this.getResourceBundle().getText(SucessoMsgCode);
				MessageBox.success(sText);
			} else {
				MessageBox.warning(WarningMessage, {
					title: "Aviso"
				});
			}
		}
	});
});