sap.ui.define([
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator"
], function (Filter, FilterOperator) {
	"use strict";

	return {

		handleVHMat: function () {
			if (!this._oValueHelpMat) {
				this._oValueHelpMat = sap.ui.xmlfragment(this.constants.pathValueHelpMaterial, this);
				this.getView().addDependent(this._oValueHelpMat);
			}
			let oSelectDialog = sap.ui.getCore().byId(this.constants.idValueHelpMaterial);
			oSelectDialog.getBinding("items").filter([]);
			this._oValueHelpMat.open();
		},

		handleVHMatSearch: function (oEvent) {
			let aFilter;
			let sQueryMat = oEvent.getSource()._searchField.getValue();
			let oSelectDialog = sap.ui.getCore().byId(this.constants.idValueHelpMaterial);

			if (sQueryMat) {
				aFilter = new Filter({
					filters: [
						new Filter("CodMaterial", FilterOperator.Contains, sQueryMat),
						new Filter("DescMaterial", FilterOperator.Contains, sQueryMat)
					],
					or: true
				});
			} else {
				aFilter = [];
			}

			oSelectDialog.getBinding("items").filter(aFilter);
		},

		handleVHMatSelect: function (oEvent) {
			let oPopupModel = this.getModel("PopupModel");
			let oModelData = oPopupModel.getData();
			let oSelectedItem = oEvent.getSource()._oList.getSelectedItem();

			oModelData.CodMaterial = oSelectedItem.getProperty("title");
			oModelData.DescMaterial = oSelectedItem.getProperty("description");
			oPopupModel.setData(oModelData);
			this.setFieldError(sap.ui.getCore().byId(this.constants.idMaterial), "");
		}

	};
});