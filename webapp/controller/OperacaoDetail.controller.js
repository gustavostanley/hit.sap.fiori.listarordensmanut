sap.ui.define([
	"hit/sap/fiori/ListarOrdensManut/controller/BaseController",
	"hit/sap/fiori/ListarOrdensManut/controller/valueHelp/VHMateriais",
	"sap/ui/core/routing/History",
	"sap/ui/model/json/JSONModel",
	"hit/sap/fiori/ListarOrdensManut/model/BarcodeScanner",
	"sap/m/MessageBox"
], function (BaseController, VHMateriais, History, JSONModel, BarcodeScanner, MessageBox) {
	"use strict";

	return BaseController.extend("hit.sap.fiori.ListarOrdensManut.controller.OperacaoDetail", {

		valueHelpMat: VHMateriais,
		stackIsReadingQR: [],

		constants: {
			idMaterial: "idCodMaterial",
			idQuantAddMat: "idQuantAddMat",
			idQuantConsumo: "idQuantConsumo",
			idValueHelpMaterial: "idSelectDialogVHMat",
			idTabelaReservas: "idMatTable",
			pathPopupAddMaterial: "hit.sap.fiori.ListarOrdensManut.view.AddMaterialPopup",
			pathPopupConsumoMaterial: "hit.sap.fiori.ListarOrdensManut.view.ConsumoMaterialPopup",
			pathValueHelpMaterial: "hit.sap.fiori.ListarOrdensManut.view.valueHelp.VHMateriais"
		},

		onInit: function () {
			this.getRouter().getRoute("OperacaoDetail").attachPatternMatched(this._onObjectMatched, this);
		},

		_onObjectMatched: function (oEvent) {
			this._OverrideMobileBackButton();

			let oDataPopup = {
				ItemReserva: "",
				CodMaterial: "",
				DescMaterial: "",
				Quantidade: 0,
				showCameraIcon: BarcodeScanner.isAvailable()
			};
			this.setModel(new JSONModel(oDataPopup), "PopupModel");

			this._sOrdem = oEvent.getParameter("arguments").OrdemNr;
			this._sOperacao = oEvent.getParameter("arguments").Operacao;
			let sPath = `/ZCDS_OPERACOES_PM(OrdemID='${ this._sOrdem }',OperacaoID='${ this._sOperacao }')`;
			this.getView().bindElement({
				path: sPath,
				model: "OperacoesModel",
				parameters: {
					expand: "to_Reserva"
				}
			});
		},

		onAddMatClick: function () {
			this.clearDataModel("PopupModel");

			let oField = sap.ui.getCore().byId(this.constants.idMaterial);
			if (oField) {
				this.setFieldError(oField, "");
			}

			this._getAddMatPopup().open();
		},

		onChangeMaterial: function () {
			let oPopupModel = this.getModel("PopupModel");
			let oPopupData = oPopupModel.getData();

			this.showLoading();
			this.getValidadorMaterial().then(oData => {
				//Preenche Dialog Model com os dados retornados do serviço
				oPopupData.CodMaterial = oData.CodMaterial;
				oPopupData.DescMaterial = oData.DescMaterial;
				oPopupModel.setData(oPopupData);
				this.closeLoading();
			}).catch(() => {
				//Limpa valores antigos
				oPopupData.DescMaterial = "";
				oPopupModel.setData(oPopupData);
				this.closeLoading();
			});
		},

		getValidadorMaterial: function () {
			let oField = sap.ui.getCore().byId(this.constants.idMaterial);
			let sValue = oField.getValue();
			let oModelMat = this.getModel("MateriaisModel");

			return new Promise((resolve, reject) => {
				//Não existe valor a ser validado
				if (sValue === "") {
					this.setFieldError(oField, "");
					resolve("NO_VALUE");
					return;
				}
				let sPath = `/ZCDS_MAT_CONSUMO('${ sValue }')`;
				//Executa leitura do Material
				oModelMat.read(sPath, {
					success: oData => {
						this.setFieldError(oField, "");
						resolve(oData);
					},
					error: oError => {
						if (oError.statusCode == "404") {
							let sErrorMaterial = this.getResourceBundle().getText("msgMaterialInvalido");
							this.setFieldError(oField, this.constants.error, sErrorMaterial);
						} else {
							this.trataError(oError);
						}
						reject(oError);
					}
				});
			});
		},

		onConsumoMatClick: function () {
			let oSelected = this.byId(this.constants.idTabelaReservas).getSelectedItem();
			let oPopupModel = this.getModel("PopupModel");
			let oPopupData = oPopupModel.getData();

			this.clearDataModel("PopupModel");

			if (!oSelected) {
				return;
			}

			let oItem = oSelected.getBindingContext("OperacoesModel").getObject();
			oPopupData.ItemReserva = oItem.ItemReserva;
			oPopupData.CodMaterial = oItem.Material;
			oPopupData.DescMaterial = oItem.DescMaterial;
			oPopupModel.setData(oPopupData);
			this._getConsumoMatPopup().open();
		},

		_getAddMatPopup: function () {
			if (!this._oAddMatDialog) {
				this._oAddMatDialog = sap.ui.xmlfragment(this.constants.pathPopupAddMaterial, this);
				this.getView().addDependent(this._oAddMatDialog);
			}
			return this._oAddMatDialog;
		},

		_getConsumoMatPopup: function () {
			if (!this._oConsomoMatDialog) {
				this._oConsomoMatDialog = sap.ui.xmlfragment(this.constants.pathPopupConsumoMaterial, this);
				this.getView().addDependent(this._oConsomoMatDialog);
			}
			return this._oConsomoMatDialog;
		},

		onConfirmarConsumo: function () {
			let oFieldQuant = sap.ui.getCore().byId(this.constants.idQuantConsumo);
			let oOperacoesModel = this.getModel("OperacoesModel");
			let oModelData = this.getModel("PopupModel").getData();
			let oContext = this.getView().getBindingContext("OperacoesModel");

			//Verifica se ainda está com erro
			if (oFieldQuant._getInput().getValueState() !== "None") {
				return;
			}

			let oEntry = {
				I_ITEM: oModelData.ItemReserva,
				I_MATERIAL: oModelData.CodMaterial,
				I_MODO: "C",
				I_NUMOPERACAO: oContext.getProperty("OperacaoID"),
				I_NUMORDEM: oContext.getProperty("OrdemID"),
				I_QUANTIDADE: oModelData.Quantidade.toString()
			};

			this.executarConsumo(oEntry).then(oData => {
				oOperacoesModel.refresh(true);
				this._RefreshPageData();
				this._RemoveTableSelections();
				this.msgSucessoOuWarning("msgSucessoConsumo", oData.E_MESSAGE);
				this._getConsumoMatPopup().close();
				this.closeLoading();
			}).catch(oError => {
				oOperacoesModel.refresh(true);
				this.trataError(oError);
				this._getConsumoMatPopup().close();
				this.closeLoading();
			});
		},

		_RemoveTableSelections: function () {
			this.byId(this.constants.idTabelaReservas).removeSelections();
		},

		onCancelarConsumo: function () {
			let oFieldQuant = sap.ui.getCore().byId(this.constants.idQuantConsumo);
			this.setFieldError(oFieldQuant._getInput(), "");
			this._getConsumoMatPopup().close();
		},

		onConfirmarAddMat: function () {
			let oFieldMat = sap.ui.getCore().byId(this.constants.idMaterial);
			let oFieldQuant = sap.ui.getCore().byId(this.constants.idQuantAddMat);
			let oOperacoesModel = this.getModel("OperacoesModel");
			let oModelData = this.getModel("PopupModel").getData();
			let oContext = this.getView().getBindingContext("OperacoesModel");

			//Verifica se ainda está com erro
			if (oFieldMat.getValueState() !== "None") {
				return;
			}

			//Verifica se ainda está com erro
			if (oFieldQuant._getInput().getValueState() !== "None") {
				return;
			}

			//Verifica Campo Obrigatório
			if (!oModelData.CodMaterial) {
				let sRequiredText = this.getResourceBundle().getText("msgRequiredText");
				this.setFieldError(oFieldMat, this.constants.error, sRequiredText);
				return;
			}

			let oEntry = {
				I_ITEM: "",
				I_MATERIAL: oModelData.CodMaterial,
				I_MODO: "I",
				I_NUMOPERACAO: oContext.getProperty("OperacaoID"),
				I_NUMORDEM: oContext.getProperty("OrdemID"),
				I_QUANTIDADE: oModelData.Quantidade.toString()
			};

			this.executarConsumo(oEntry).then(oData => {
				this._RefreshPageData();
				oOperacoesModel.refresh(true);
				this.msgSucessoOuWarning("msgSucessoAddMaterial", oData.E_MESSAGE);
				this._getAddMatPopup().close();
				this.closeLoading();
			}).catch(oError => {
				oOperacoesModel.refresh(true);
				this.trataError(oError);
				this._getAddMatPopup().close();
				this.closeLoading();
			});
		},

		onCancelarAddMat: function () {
			let oFieldMat = sap.ui.getCore().byId(this.constants.idMaterial);
			let oFieldQuant = sap.ui.getCore().byId(this.constants.idQuantAddMat);
			this.setFieldError(oFieldMat, "");
			this.setFieldError(oFieldQuant._getInput(), "");
			this._getAddMatPopup().close();
		},

		executarConsumo: function (oEntry) {
			return new Promise((resolve, reject) => {
				let oTModel = this.getModel("TransactionalModel");

				this.showLoading();
				oTModel.create("/CreateConsumo", oEntry, {
					success: oData => {
						resolve(oData);
					},
					error: oError => {
						reject(oError);
					}
				});
			});
		},

		_RefreshPageData: function () {
			// this.getModel().resetChanges();
			this.getModel().refresh(true);
		},

		onNavBack: function () {
			document.removeEventListener("backbutton", this.onBackButtonWithContext, true);

			let oHistory = History.getInstance();
			let sPreviousHash = oHistory.getPreviousHash();

			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				this.getRouter().navTo("OrdemDetail", {
					OrdemNr: this._sOrdem
				}, true);
			}
		},

		onReadMaterial: function () {
			this.lockBackButton();
			this.getReader().then(sMaterial => {
				this.unLockBackButton();
				sap.ui.getCore().byId(this.constants.idMaterial).setValue(sMaterial);
				this.onChangeMaterial();
			}).catch(oError => {
				this.unLockBackButton();
				if (oError.error === "NOT_SUPPORTED") {
					let sErrorMessage = this.getResourceBundle().getText("msgBarcodeNotPossible");
					MessageBox.error(sErrorMessage);
				}
			});
		},

		getReader: function () {
			return new Promise((resolve, reject) => {
				BarcodeScanner.scan(
					mResult => {
						if (!mResult.cancelled) {
							resolve(mResult.text);
						} else {
							reject(mResult);
						}
					},
					mError => {
						reject(mError);
					}
				);
			});
		},

		lockBackButton: function () {
			this.stackIsReadingQR.push(1);
		},

		unLockBackButton: function () {
			setTimeout(() => {
				this.stackIsReadingQR.pop(1);
			}, 500);
		},

		_OverrideMobileBackButton: function () {
			this.onBackButtonWithContext = this.onBackKeyDown.bind(this);

			document.addEventListener("backbutton", this.onBackButtonWithContext, true);
		},

		onBackKeyDown: function () {
			if (this.stackIsReadingQR.length !== undefined && this.stackIsReadingQR.length !== 0) {
				return;
			}
			this.onNavBack();
		},

		onExit: function () {
			document.removeEventListener("backbutton", this.onBackButtonWithContext, true);
		}

	});

});