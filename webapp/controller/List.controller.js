sap.ui.define([
	"hit/sap/fiori/ListarOrdensManut/controller/BaseController",
	"sap/m/MessageToast",
	"hit/sap/fiori/ListarOrdensManut/model/formatter",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator"
], function (BaseController, MessageToast, formatter, Filter, FilterOperator) {
	"use strict";

	return BaseController.extend("hit.sap.fiori.ListarOrdensManut.controller.List", {
		formatter: formatter,

		onInit: function () {
			this.getRouter().getRoute("List").attachPatternMatched(this._onObjectMatched, this);
		},

		_onObjectMatched: function () {
			this.getModel().refresh(true);
		},

		onItemPress: function (oEvent) {
			let sKey = oEvent.getSource().getBindingContext().getProperty("OrdemID");
			this.getRouter().navTo("OrdemDetail", {
				OrdemNr: sKey
			});
		},

		onRefresh: function () {
			this.showLoading();
			this.getModel().refresh(true);
			this.getModel().attachRequestCompleted(() => {
				this.byId("idRefreshList").hide();
				this.closeLoading();
			});
		},

		onChangeSearch: function (oEvent) {
			var aFilter = [];
			var sQuery = oEvent.getSource().getValue();
			if (sQuery) {
				aFilter = new Filter({
					filters: [
						new Filter("OrdemID", FilterOperator.Contains, sQuery),
						new Filter("to_Equipamento/DescricaoEquipamento", FilterOperator.Contains, sQuery)
					],
					or: true
				});
			}
			// filter binding
			var oTable = this.byId("idOrdensTable");
			var oBinding = oTable.getBinding("items");
			oBinding.filter(aFilter);
		},

		onNavBack: function () {
			window.history.go(-1);
		}
	});
});