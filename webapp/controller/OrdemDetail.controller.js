sap.ui.define([
	"hit/sap/fiori/ListarOrdensManut/controller/BaseController",
	"hit/sap/fiori/ListarOrdensManut/controller/valueHelp/VHLocal",
	"hit/sap/fiori/ListarOrdensManut/controller/valueHelp/VHEquip",
	"hit/sap/fiori/ListarOrdensManut/model/BarcodeScanner",
	"hit/sap/fiori/ListarOrdensManut/model/formatter",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageBox"
], function (BaseController, VHLocal, VHEquip, BarcodeScanner, formatter, JSONModel, MessageBox) {
	"use strict";

	return BaseController.extend("hit.sap.fiori.ListarOrdensManut.controller.OrdemDetail", {
		formatter: formatter,
		valueHelpLocal: VHLocal,
		valueHelpEquip: VHEquip,
		stackIsReadingQR: [],

		constants: {
			idValueHelpLocal: "idSelectDialogVHLocal",
			pathValueHelpLocal: "hit.sap.fiori.ListarOrdensManut.view.valueHelp.VHLocal",
			idValueHelpEquip: "idSelectDialogVHEquip",
			pathValueHelpEquip: "hit.sap.fiori.ListarOrdensManut.view.valueHelp.VHEquip",
			idLocalInstEdit: "idLocalEdit",
			idDescLocalInstEdit: "idDescLocalEdit",
			idEquipamentoEdit: "idEquipamentoEdit",
			idDescEquipamentoEdit: "idDescEquipamentoEdit",
			idAnomaliaEdit: "idAnomEdit",
			StatusSUMO: "SUMO",
			StatusSUMT: "SUMT",
			StatusSUFM: "SUFM",
			StatusINIC: "INIC",
			StatusSUUM: "SUUM",
			StatusENCR: "ENCR",
			CentroTrabalhoQualidade: "M-QA"
		},

		onInit: function () {
			this.getRouter().getRoute("OrdemDetail").attachPatternMatched(this._onObjectMatched, this);
		},

		_onObjectMatched: function (oEvent) {
			this._OverrideMobileBackButton();

			//Criando model para as dialogs
			let oData = {
				LocalID: "",
				DescricaoLocal: "",
				EquipamentoID: "",
				DescricaoEquipamento: "",
				AnomaliaID: "",
				showCameraIcon: BarcodeScanner.isAvailable()
			};

			let oDialogModel = new JSONModel(oData);
			this.setModel(oDialogModel, "DialogModel");

			//Efetua Bind do Ecrã
			let sPath = `/ZCDS_LISTAGEM_ORDENS_PM('${ oEvent.getParameter("arguments").OrdemNr }')`;
			this.getView().bindElement({
				path: sPath,
				parameters: {
					expand: "to_Equipamento,to_Nota,to_LocalInstalacao"
				},
				events: {
					dataReceived: this._PreencheViewData.bind(this),
					change: this._PreencheViewData.bind(this)
				}
			});
		},

		_PreencheViewData: function () {
			//Preenchimento do valor inicial do form
			let oBindingContext = this.getView().getBindingContext();
			let sDescricaoEquipamento = "";
			let sDescricaoLocal = "";

			let oLocalContext = this.getModel().getProperty(`${ oBindingContext.getPath() }/to_LocalInstalacao`);
			if (oLocalContext) {
				sDescricaoLocal = oLocalContext.DescricaoLocal;
			}

			let oEquipContext = this.getModel().getProperty(`${ oBindingContext.getPath() }/to_Equipamento`);
			if (oEquipContext) {
				sDescricaoEquipamento = oEquipContext.DescricaoEquipamento;
			}

			//Criando model para o formulario
			let oViewData = {
				LocalID: oBindingContext.getProperty("LocalID"),
				DescricaoLocal: sDescricaoLocal,
				EquipamentoID: oBindingContext.getProperty("EquipamentoID"),
				DescricaoEquipamento: sDescricaoEquipamento,
				AnomaliaID: oBindingContext.getProperty("to_Nota/AnomaliaID"),
				DescAnomalia: oBindingContext.getProperty("to_Nota/DescAnomalia"),
				Parada: oBindingContext.getProperty("ContactoProduto"),
				CentroTrabalhoQualidade: this.constants.CentroTrabalhoQualidade,
				EnableBtnConf: false,
				EnableBtnINIC: true,
				EnableBtnENCR: true,
				EnableBtnSUMO: true,
				EnableBtnSUMT: true,
				EnableBtnSUFM: true,
				EnableBtnSUUM: true
			};

			this.updateStatusBtn(oViewData, oBindingContext);

			let oViewModel = new JSONModel(oViewData);
			this.setModel(oViewModel, "viewModel");
		},

		_RefreshPageData: function () {
			// this.getModel().resetChanges();
			this.getModel().refresh(true);
		},

		onOperacaoPress: function (oEvent) {
			document.removeEventListener("backbutton", this.onBackButtonWithContext, true);
			let sOrdem = oEvent.getSource().getBindingContext().getProperty("OrdemID");
			let sOperacao = oEvent.getSource().getBindingContext().getProperty("OperacaoID");
			this.getRouter().navTo("OperacaoDetail", {
				OrdemNr: sOrdem,
				Operacao: sOperacao
			});
		},

		onNavBack: function () {
			document.removeEventListener("backbutton", this.onBackButtonWithContext, true);
			this.navBack("List");
		},

		onChangeEquipPress: function () {
			let oViewData = this.getModel("viewModel").getData();
			let oDialogData = this.getModel("DialogModel").getData();

			//Codigo do Equipamento
			oDialogData.EquipamentoID = oViewData.EquipamentoID;
			//Descrição do Equipamento
			oDialogData.DescricaoEquipamento = oViewData.DescricaoEquipamento;

			this.getModel("DialogModel").setData(oDialogData);
			this._getEquipDialog().open();
		},

		onChangeEquipamento: function () {
			let oDialogModel = this.getModel("DialogModel");
			let oDialogData = oDialogModel.getData();

			this.showLoading();
			this.getValidadorEquipamento().then(oData => {

				if (oData === "NO_VALUE") {
					oDialogData.EquipamentoID = "";
					oDialogData.DescricaoEquipamento = "";
					oDialogModel.setData(oDialogData);
					this.closeLoading();
					return;
				}

				//Preenche Dialog Model com os dados retornados do serviço
				oDialogData.EquipamentoID = oData.EquipamentoID;
				oDialogData.DescricaoEquipamento = oData.DescricaoEquipamento;

				if (oData.to_LocalInstalacao !== null) {
					oDialogData.LocalID = oData.to_LocalInstalacao.LocalID;
					oDialogData.DescricaoLocal = oData.to_LocalInstalacao.DescricaoLocal;
				} else {
					oDialogData.LocalID = "";
					oDialogData.DescricaoLocal = "";
				}

				oDialogModel.setData(oDialogData);
				this.closeLoading();
			}).catch(() => {
				//Limpa valores antigos
				oDialogData.DescricaoEquipamento = "";
				oDialogData.LocalID = "";
				oDialogData.DescricaoLocal = "";
				oDialogModel.setData(oDialogData);
				this.closeLoading();
			});
		},

		getValidadorEquipamento: function () {
			let oField = sap.ui.getCore().byId(this.constants.idEquipamentoEdit);
			let sValue = oField.getValue().trim();
			let oModelEqui = this.getModel("EquipamentosModel");

			return new Promise((resolve, reject) => {
				//Não existe valor a ser validado
				if (sValue === "") {
					this.setFieldError(oField, "");
					resolve("NO_VALUE");
					return;
				}
				let sPath = `/ZCDS_EQUIPAMENTO('${ sValue }')`;
				//Executa leitura do Equipamento
				oModelEqui.read(sPath, {
					urlParameters: {
						"$expand": "to_LocalInstalacao"
					},
					success: oData => {
						this.setFieldError(oField, "");
						resolve(oData);
					},
					error: oError => {
						if (oError.statusCode == "404") {
							let sErrorEquipamento = this.getResourceBundle().getText("msgEquipamentoInvalido");
							this.setFieldError(oField, this.constants.error, sErrorEquipamento);
						} else {
							this.trataError(oError);
						}
						reject(oError);
					}
				});
			});
		},

		onConfirmarEditEquip: function () {
			let oEquiIdField = sap.ui.getCore().byId(this.constants.idEquipamentoEdit);
			let oDialogData = this.getModel("DialogModel").getData();
			let oViewModel = this.getModel("viewModel");
			let oViewData = oViewModel.getData();

			//Verifica se ainda está com erro
			if (oEquiIdField.getValueState() !== "None") {
				return;
			}

			let sEquipamentoIDOld = oViewData.EquipamentoID;

			//Verifica se não foi alterado o Equipamento
			if (sEquipamentoIDOld === oDialogData.EquipamentoID) {
				this._getEquipDialog().close();
				this.clearDataModel("DialogModel");
				return;
			}

			oViewData.EnableBtnConf = true;

			//Preenche Equipamento e Descrição
			oViewData.EquipamentoID = oDialogData.EquipamentoID;
			oViewData.DescricaoEquipamento = oDialogData.DescricaoEquipamento;

			// Preenche Local de Instalação
			oViewData.LocalID = oDialogData.LocalID;
			oViewData.DescricaoLocal = oDialogData.DescricaoLocal;

			oViewModel.setData(oViewData);
			this._getEquipDialog().close();
		},

		onCancelarEditEquip: function () {
			this.clearDataModel("DialogModel");
			this.setFieldError(sap.ui.getCore().byId(this.constants.idEquipamentoEdit), "");
			this._getEquipDialog().close();
		},

		_getEquipDialog: function () {
			if (!this._oEquipDialog) {
				this._oEquipDialog = sap.ui.xmlfragment("hit.sap.fiori.ListarOrdensManut.view.EditEquipPopup", this);
				this.getView().addDependent(this._oEquipDialog);
			}
			return this._oEquipDialog;
		},

		onChangeLocalPress: function () {
			let oDialogModel = this.getModel("DialogModel");
			let oDialogData = oDialogModel.getData();
			let oViewModel = this.getModel("viewModel");
			let oViewData = oViewModel.getData();

			//Codigo do Local de Instalacao
			oDialogData.LocalID = oViewData.LocalID;
			//Descrição do Local
			oDialogData.DescricaoLocal = oViewData.DescricaoLocal;

			oDialogModel.setData(oDialogData);
			this._getLocalDialog().open();
		},

		onChangeLocal: function () {
			let oDialogModel = this.getModel("DialogModel");
			let oDialogData = oDialogModel.getData();

			this.showLoading();
			this.getValidadorLocal().then(oData => {
				//Preenche Dialog Model com os dados retornados do serviço
				oDialogData.LocalID = oData.LocalID;
				oDialogData.DescricaoLocal = oData.DescricaoLocal;

				oDialogModel.setData(oDialogData);
				this.closeLoading();
			}).catch(() => {
				//Limpa valores antigos
				oDialogData.DescricaoLocal = "";
				oDialogModel.setData(oDialogData);
				this.closeLoading();
			});
		},

		getValidadorLocal: function () {
			let oField = sap.ui.getCore().byId(this.constants.idLocalInstEdit);
			let sValue = oField.getValue().trim();
			let oModelLocal = this.getModel("LocalModel");

			return new Promise((resolve, reject) => {
				//Não existe valor a ser validado
				if (sValue === "") {
					this.setFieldError(oField, "");
					resolve("NO_VALUE");
					return;
				}
				//Serviço para validação dos valores
				let sPath = `/ZCDS_GET_FUNC_LOCATION('${ sValue }')`;
				//Executa leitura do Local
				oModelLocal.read(sPath, {
					success: oData => {
						this.setFieldError(oField, "");
						resolve(oData);
					},
					error: oError => {
						if (oError.statusCode == "404") {
							let sErrorLocal = this.getResourceBundle().getText("msgLocalInvalido");
							this.setFieldError(oField, this.constants.error, sErrorLocal);
						} else {
							this.trataError(oError);
						}
						reject(oError);
					}
				});
			});
		},

		onConfirmarEditLocal: function () {
			let oLocalIDField = sap.ui.getCore().byId(this.constants.idLocalInstEdit);
			let oDialogData = this.getModel("DialogModel").getData();
			let oViewModel = this.getModel("viewModel");
			let oViewData = oViewModel.getData();

			//Verifica se ainda está com erro
			if (oLocalIDField.getValueState() !== "None") {
				return;
			}

			let sLocalIDOld = oViewData.LocalID;

			//Verifica se não foi alterado o Local de Instalação
			if (sLocalIDOld === oDialogData.LocalID) {
				this._getLocalDialog().close();
				this.clearDataModel("DialogModel");
				return;
			}

			oViewData.EnableBtnConf = true;

			//Preenche Local e Descrição
			oViewData.LocalID = oDialogData.LocalID;
			oViewData.DescricaoLocal = oDialogData.DescricaoLocal;

			//Limpa Equipamento
			oViewData.EquipamentoID = "";
			oViewData.DescricaoEquipamento = "";

			oViewModel.setData(oViewData);
			this._getLocalDialog().close();
		},

		onCancelarEditLocal: function () {
			this.clearDataModel("DialogModel");
			this.setFieldError(sap.ui.getCore().byId(this.constants.idLocalInstEdit), "");
			this._getLocalDialog().close();
		},

		_getLocalDialog: function () {
			if (!this._oLocalDialog) {
				this._oLocalDialog = sap.ui.xmlfragment("hit.sap.fiori.ListarOrdensManut.view.EditLocalPopup", this);
				this.getView().addDependent(this._oLocalDialog);
			}
			return this._oLocalDialog;
		},

		onChangeAnomPress: function () {
			let oDialogModel = this.getModel("DialogModel");
			let oDialogData = oDialogModel.getData();
			let oViewModel = this.getModel("viewModel");
			let oViewData = oViewModel.getData();

			//Codigo do Local de Instalacao
			oDialogData.AnomaliaID = oViewData.AnomaliaID;

			oDialogModel.setData(oDialogData);
			this._getAnomDialog().open();
		},

		onConfirmarEditAnom: function () {
			let oAnomaliaIDField = sap.ui.getCore().byId(this.constants.idAnomaliaEdit);
			let oViewModel = this.getModel("viewModel");
			let oViewData = oViewModel.getData();

			let sKey = oAnomaliaIDField.getSelectedItem().getProperty("key");
			let sText = oAnomaliaIDField.getSelectedItem().getProperty("text");

			//Verifica Campo Obrigatório
			if (!sKey) {
				let sRequiredText = this.getResourceBundle().getText("msgRequiredText");
				this.setFieldError(oAnomaliaIDField, this.constants.error, sRequiredText);
				return;
			}

			oViewData.EnableBtnConf = true;

			oViewData.AnomaliaID = sKey;
			oViewData.DescAnomalia = sText;

			oViewModel.setData(oViewData);
			this._getAnomDialog().close();
		},

		onCancelarEditAnom: function () {
			this.clearDataModel("DialogModel");
			this._getAnomDialog().close();
		},

		_getAnomDialog: function () {
			if (!this._oAnomDialog) {
				this._oAnomDialog = sap.ui.xmlfragment("hit.sap.fiori.ListarOrdensManut.view.EditAnomPopup", this);
				this.getView().addDependent(this._oAnomDialog);
			}
			return this._oAnomDialog;
		},

		onConfirmarAlteracao: function () {
			let sMessage = this.getResourceBundle().getText("questConfirmarAlteracao");
			let oData = this.getModel("viewModel").getData();

			//Verifica Campo Obrigatório
			if (!oData.LocalID) {
				let labelLocal = this.getResourceBundle().getText("lblLocal");
				MessageBox.error(this.getResourceBundle().getText("msgCampoObrigatorio", labelLocal));
				return;
			}

			this.confirmarAlteracao(sMessage, this.executaAlteracao.bind(this));
		},

		executaAlteracao: function () {
			let oBindingContext = this.getView().getBindingContext();
			let oViewData = this.getModel("viewModel").getData();
			let oTModel = this.getModel("TransactionalModel");

			let oEntry = {
				I_CODING: oViewData.AnomaliaID,
				I_DESCCODING: "",
				I_EQUNR: oViewData.EquipamentoID,
				I_NUMORD: oBindingContext.getProperty("OrdemID"),
				I_TPLNR: oViewData.LocalID
			};

			this.showLoading();
			oTModel.create("/UpdateOrdem", oEntry, {
				success: oData => {
					this._RefreshPageData();
					this.msgSucessoOuWarning("msgSucessoAlteracao", oData.E_MESSAGE);
					this.closeLoading();
				},
				error: oError => {
					this.trataError(oError);
					this.closeLoading();
				}
			});
		},

		confirmarAlteracao: function (sPergunta, callbackFunction) {
			MessageBox.confirm(sPergunta, {
				icon: sap.m.MessageBox.Icon.INFORMATION,
				title: this.getResourceBundle().getText("titleChangeOrdem"),
				actions: [MessageBox.Action.YES, MessageBox.Action.NO],
				onClose: function (oAction) {
					if (oAction === MessageBox.Action.YES) {
						callbackFunction();
					}
				}
			});
		},

		onClickIniciar: function () {
			this.confirmarAlteracao(this.getResourceBundle().getText("questIniciar"), () => {
				this.alterarStatus(this.constants.StatusINIC);
			});
		},

		onClickEncerrar: function () {
			this.confirmarAlteracao(this.getResourceBundle().getText("questEncerrar"), () => {
				this.alterarStatus(this.constants.StatusENCR);
			});
		},

		onClickSuspFM: function () {
			this.confirmarAlteracao(this.getResourceBundle().getText("questSuspFM"), () => {
				this.alterarStatus(this.constants.StatusSUFM);
			});
		},

		onClickSuspMT: function () {
			this.confirmarAlteracao(this.getResourceBundle().getText("questSuspMT"), () => {
				this.alterarStatus(this.constants.StatusSUMT);
			});
		},

		onClickSuspFMO: function () {
			this.confirmarAlteracao(this.getResourceBundle().getText("questSuspFMO"), () => {
				this.alterarStatus(this.constants.StatusSUMO);
			});
		},

		onClickSuspUM: function () {
			this.confirmarAlteracao(this.getResourceBundle().getText("questSuspUM"), () => {
				this.alterarStatus(this.constants.StatusSUUM);
			});
		},

		alterarStatus: function (sStatus) {
			let oBindingContext = this.getView().getBindingContext();
			let oTModel = this.getModel("TransactionalModel");

			let oEntry = {
				I_NUMORDEM: oBindingContext.getProperty("OrdemID"),
				I_USER_STATUS: sStatus
			};
			this.showLoading();
			oTModel.create("/UpdateStatusOrdem", oEntry, {
				success: oData => {
					this._RefreshPageData();
					this.msgSucessoOuWarning("msgSucessoStatus", oData.E_MESSAGE);
					this.closeLoading();
				},
				error: oError => {
					this.trataError(oError);
					this.closeLoading();
				}
			});
		},

		updateStatusBtn: function (data, context) {
			var sStatus = context.getProperty("Estado");
			if (sStatus.includes("ENCR")) {
				data.EnableBtnINIC = false;
				data.EnableBtnENCR = false;
				data.EnableBtnSUMO = false;
				data.EnableBtnSUMT = false;
				data.EnableBtnSUFM = false;
				data.EnableBtnSUUM = false;
			} else {
				if (sStatus.includes("SUFM") || sStatus.includes("SUMO") || sStatus.includes("SUMT") || sStatus.includes("SUUM")) {
					data.EnableBtnINIC = true;

					if (sStatus.includes("SUMO")) {
						data.EnableBtnSUMO = false;
					} else {
						data.EnableBtnSUMO = true;
					}
					if (sStatus.includes("SUMT")) {
						data.EnableBtnSUMT = false;
					} else {
						data.EnableBtnSUMT = true;
					}
					if (sStatus.includes("SUFM")) {
						data.EnableBtnSUFM = false;
					} else {
						data.EnableBtnSUFM = true;
					}
					if (sStatus.includes("SUUM")) {
						data.EnableBtnSUUM = false;
					} else {
						data.EnableBtnSUUM = true;
					}
				} else {
					if (sStatus.includes("INIC")) {
						data.EnableBtnINIC = false;
					}
				}
			}
		},

		lockBackButton: function () {
			this.stackIsReadingQR.push(1);
		},

		unLockBackButton: function () {
			setTimeout(() => {
				this.stackIsReadingQR.pop(1);
			}, 500);
		},

		onReadLocal: function () {
			this.lockBackButton();

			this.getReader().then(sLocal => {
				this.unLockBackButton();
				sap.ui.getCore().byId(this.constants.idLocalInstEdit).setValue(sLocal);
				this.onChangeLocal();
			}).catch(oError => {
				this.unLockBackButton();
				if (oError.error === "NOT_SUPPORTED") {
					let sErrorMessage = this.getResourceBundle().getText("msgBarcodeNotPossible");
					MessageBox.error(sErrorMessage);
				}
			});
		},

		onReadEquipamento: function () {
			this.lockBackButton();

			this.getReader().then(sEquip => {
				this.unLockBackButton();
				sap.ui.getCore().byId(this.constants.idEquipamentoEdit).setValue(sEquip);
				this.onChangeEquipamento();
			}).catch(oError => {
				this.unLockBackButton();
				if (oError.error === "NOT_SUPPORTED") {
					let sErrorMessage = this.getResourceBundle().getText("msgBarcodeNotPossible");
					MessageBox.error(sErrorMessage);
				}
			});
		},

		getReader: function () {
			return new Promise((resolve, reject) => {
				BarcodeScanner.scan(
					mResult => {
						if (!mResult.cancelled) {
							resolve(mResult.text);
						} else {
							reject(mResult);
						}
					},
					mError => {
						reject(mError);
					}
				);
			});
		},

		_OverrideMobileBackButton: function () {
			this.onBackButtonWithContext = this.onBackKeyDown.bind(this);

			document.addEventListener("backbutton", this.onBackButtonWithContext, true);
		},

		onBackKeyDown: function () {
			if (this.stackIsReadingQR.length !== undefined && this.stackIsReadingQR.length !== 0) {
				return;
			}
			this.onNavBack();
		},

		onExit: function () {
			document.removeEventListener("backbutton", this.onBackButtonWithContext, true);
		}

	});
});